# Final DeCharla

# ENTREGA CONVOCATORIA EXTRAORDINARIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Yeray Terradillos Lorenzo
* Titulación: Ingenieria Telematica
* Cuenta en laboratorios: yeraytl
* Cuenta URJC: y.terradillos.2018@alumnos.urjc.es
* Video básico (url): https://www.youtube.com/watch?v=U99up8P_CNA
* Video parte opcional (url): https://www.youtube.com/watch?v=GO48UAwlWqw
* Despliegue (url): yeraytl.pythonanywhere.com/login
* Contraseñas: 123456 y 1234
* Cuenta Admin Site: usuario/contraseña: admin/admin

## Resumen - Descripción general

Este documento detalla una aplicación de chat que brinda a las sesiones identificadas la capacidad de
comunicarse en tiempo real. Para acceder a los recursos de la aplicación, se requiere autenticación mediante una
contraseña válida.

Después de poner la contraseña en el área de "Inicio de sesión", los usuarios son redirigidos a la página
de inicio (HOME), la cual va a tener lo siguiente:

- Varias opciones de navegación (CHATS, AYUDA, CONFIGURACIÓN y ADMINISTRADOR).
- Tendre una barra lateral que permite crear salas solo en la pagina principal.
- Un pie de página que muestra el recuento de mensajes totales, salas activas y número de imágenes totales.
  Este pie de página estará visible en todas las páginas de la aplicación.
- Una lista de las salas activas con enlaces a su estado estático, dinámico y JSON.

Cuando entramos a la pagina de un chat en la parte de abajo vamos a tener dos botones para acceder al chat en
formato dinamico y en formato JSON. Al hacer clic en cada una de estas etiquetas, se redirigirá a la misma
pagina pero añadiendo la funcionalidad descrita. También habrá un botón para acceder al tipo de chat correspondiente. 
Una vez dentro de una sala de CHAT, ya sea estática o dinámica, se puede enviar un mensaje o una imagen.
Para enviar una imagen, se debe incluir la URL en el formulario y hacer clic en el botón correspondiente que
checkea si el mensaje es una imagen o simplemente es texto. 

Además, en las salas estáticas de CHAT, se puede realizar una solicitud "PUT" que incluye mensajes en
formato "XML". Esta solicitud se hara a traves de la aplicacion REST en la cual pondremos que queremos hacer una petcion
PUT a la direccion URL de la sala en la que queremos enviar el mensaje que tendra que tener formato XML para poder parsearlo
adecuadamente y poder coger la informacion que necesitamos para añadir el mensaje a la sala.

Los campos requeridos en el "XML" son los siguientes:

- Encabezado del formato `XML`.
- Campo `messages`: Contiene los mensajes que se desean incluir.
- Campo `message`: Contiene el mensaje a incluir y debe incluir la etiqueta "isimg", que puede ser "True" si el 
  mensaje es una imagen o "False" si el mensaje es texto.
- Campo `text`: Contiene el texto del mensaje o la URL de la imagen.

Al hacer clic en el botón AYUDA, se muestra una página con secciones desplegables que explican el funcionamiento 
de cada recurso de la aplicación.

Al hacer clic en el botón CONFIGURACIÓN, se muestra una página donde se puede personalizar el nombre de 
la sesión, el tipo de fuente y el tamaño de la fuente de la aplicación.

Finalmente, al hacer clic en el botón ADMINISTRADOR, se abre la interfaz del "Admin Site" característico de cualquier aplicación DJANGO.

Además de los botones de navegación, en la barra lateral derecha se puede hacer clic en el botón "CREAR CHAT", que redirige a una página
donde se puede crear una sala con un nombre elegido por el usuario.

En conclusion, esta aplicación de chat permite a los usuarios participar en salas de chat, enviar mensajes de texto y compartir
imágenes.

## Lista de partes opcionales


* Salas Favoritas: En este apartado he añadido un boton de LIKE para cada sala con este boton podras dar like a una sala
  indicando que te gusta la conversacion de esta sala en concreto. 

* Borrar Salas: En este apartado opcional, el usuario va a poder eliminar cualquiera de los Chat creados por él a
  partir de un botón que se encontrara al lado del boton de like.

* Inclusión del Favicon: En este apartado opcional, se incluye un favicon en todos los recursos de nuestra aplicación.

* Permitir que las sesiones autenticadas se puedan terminar
