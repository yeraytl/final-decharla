from django.db import models


# Create your models here.
class Password(models.Model):
    Password = models.CharField(max_length=16)

    def __str__(self):
        return "Una de las claves válidas con identificador " + str(self.id) + " es: " + str(self.key)


class Sesion(models.Model):
    Sesion = models.TextField(unique=True, null=True)
    Nombre = models.TextField(default="anonimo")
    font_size = models.CharField(max_length=10, default="small")
    font_type = models.CharField(max_length=20, default="sans-serif")

    def __str__(self):
        return "El charlador es: " + str(self.Nombre) +\
               " con sesion: " + str(self.Sesion) + " además tenemos un font_size y un font_type" +\
               str(self.font_size) + str(self.font_type)


class Sala(models.Model):
    Nombre = models.CharField(max_length=64)
    Autor = models.ForeignKey(Sesion, on_delete=models.CASCADE)
    Votos = models.PositiveIntegerField(default=0)

    def __str__(self):
        return "El nombre de la sala es: " + str(self.Nombre) +\
               " con autor: " + str(self.Autor) + " además tenemos un número de votos en cada sala:" +\
               str(self.Votos)


class Mensaje(models.Model):
    Fecha = models.DateTimeField(auto_now_add=True)
    Texto = models.TextField()
    is_image = models.BooleanField(default=False)
    is_image2 = models.BooleanField(default=False)
    imagen = models.ImageField(upload_to='imagenes/', null=True, blank=True)
    imagen2 = models.URLField(null=True, blank=True)
    Autor = models.ForeignKey(Sesion, on_delete=models.CASCADE)
    Sala = models.ForeignKey(Sala, on_delete=models.CASCADE)

    def __str__(self):
        return "El texto de el mensaje es: " + str(self.Texto) +\
               "Con fecha: " + str(self.Fecha) + \
               "Tenemos un booleano para ver si el mensaje es imagen: " + str(self.is_image2) + \
               "El autor es: " + str(self.Autor) + \
               "El mensaje pertenecerá a la sala: " + str(self.Sala)
