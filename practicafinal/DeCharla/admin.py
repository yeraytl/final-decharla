from django.contrib import admin
from .models import Password, Sesion, Sala, Mensaje
# Register your models here.

admin.site.register(Sesion)
admin.site.register(Password)
admin.site.register(Sala)
admin.site.register(Mensaje)
