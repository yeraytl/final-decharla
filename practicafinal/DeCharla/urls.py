from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('login', views.login, name='login'),
    path('', views.index, name='principal'),
    path('logout/', views.logout, name='logout'),
    path('ayuda', views.help, name='ayuda'),
    path('configuracion', views.config, name='configuracion'),
    path('favicon.ico', views.favicon, name='favicon'),
    path('busca_paginas', views.buscaPaginas, name='busca_paginas'),
    path('<str:id_sala>', views.sala, name='sala'),
    path('<str:id_sala>/din', views.salaDinamica, name='saladin'),
    path('<str:id_sala>/json', views.salaJson, name='sala_json'),
    path('borrar/<int:id_sala>', views.deleteSala, name='borrar_sala'),
    path('votar/<int:id_sala>', views.voto, name='votar'),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


