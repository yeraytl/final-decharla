import random
import json
import xml.etree.ElementTree as ET
import string

from django.core import validators
from django.core.exceptions import ValidationError
from django.http import HttpResponse
from django.shortcuts import render, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie
from .models import Sesion, Sala, Mensaje, Password
from django.utils import timezone
import datetime

#Funcion para obtener el numero total de interacciones del chat y ponerlo en la parte baja de la pagina
def get_estadisticas():
    mensajes_imagen = Mensaje.objects.filter(is_image=True).count()
    salas_activas = Sala.objects.count()
    mensajes_texto = Mensaje.objects.filter(is_image=False).count()

    return mensajes_texto, mensajes_imagen, salas_activas

#Función para parsear un mensaje en formato xml que recibamos
def xml_parser(xml_string):
    root = ET.fromstring(xml_string)
    messages = []
    for msg in root.findall('message'):
        isimg = msg.get('isimg')
        if isimg == "true":
            is_img = True
        else:
            is_img = False
        text = msg.find('text').text
        messages.append({'isimg': is_img, 'text': text})
    return messages

#Función para obtener una lista con todas las salas
def get_sala_lista(request):
    lista_salas = Sala.objects.all() #Obtenemos todods los objetos de tipo sala
    for sala in lista_salas:         #Recorremos la lista de salas
        total_messages = Mensaje.objects.filter(Sala=sala).count() #Obtenemos el total de mensajes de la sala
        last_visit = request.session.get(f"last_visit_{sala.Nombre}") #Obtenemos el número de mensajes desde la ultima visita
        if last_visit:
            if isinstance(last_visit, str):
                last_visit = datetime.datetime.strptime(last_visit, "%Y-%m-%d %H:%M:%S")  # Convertir a datetime
            last_visit_messages = Mensaje.objects.filter(Sala=sala, Fecha__gt=last_visit).exclude(Autor__Sesion=request.COOKIES.get("Sesion")).count()
        else:
            last_visit_messages = total_messages
        sala.total_messages = total_messages #Incluimos dentro de la sala el número total de mensajes
        sala.last_visit_messages = last_visit_messages #Incluimos dentro de la sala el número total de mensajes desde la ultima visita
        request.session[f"last_visit_{sala.Nombre}"] = timezone.now().strftime("%Y-%m-%d %H:%M:%S")
    return lista_salas


#Función que checkea si la sesión esta iniciada o no esrá iniciada
def checkSession(request):
    cookie = request.COOKIES.get("Sesion") #Obtenmos la cookie de sesion
    if cookie is None: #Si la cookie esta vacia...
        cookie = ''.join(random.choices(string.ascii_lowercase + string.digits, k=128)) #Creamos la cookie aleatoria
        response = HttpResponseRedirect("/login") #Hacemos redirección a la página de login
        response.set_cookie("Sesion", cookie) #Hacemos SET de la cookie
        return response
    if request.path != "/login": #Si el path en el que estamos es distinto de login
        check = Sesion.objects.filter(Sesion=cookie).exists() # Mirar a ver si tenemos una cookie de sesion será un booleano
    else:
        check = True #Si estamos en la pagina de login ponemos check a True
    if not check: #Si check es False entonces hacemos redireccion a login
        return HttpResponseRedirect("/login")
    return None

#Lo que hace csrf_exempt es saltar el mecanismo de seguridad por ejemplo si no tenemos esto, en el index y intentamos hacer un post en esta pagina no vamos a poder
@ensure_csrf_cookie
@csrf_exempt
def index(request): #Función para controlar la página principal
    response = checkSession(request) #Hacemos el checkeo de si esta la sesion iniciada o no
    if response:
        return response

    if request.method == "POST": # Si el metodo es POST
        sala_nombre = request.POST.get("sala") #Obtenemos el POST que hemos hecho con el nombre sala
        nombre_existente = Sala.objects.filter(Nombre=sala_nombre).exists() #Miramos si el nombre existe dentro de las salas creadas
        if not nombre_existente: #Si el nombre no existe...
            sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion")) #Obtenemos la cookie de sesión
            sala_auxiliar = Sala(Nombre=sala_nombre, Autor=sesion) #Creamos una sala auxiliar
            sala_auxiliar.save() #Guardamos esta sala dentro de la base de datos
            sala_html = HttpResponseRedirect("/" + sala_nombre) #Redireccionamos a la sala
            return sala_html

    mensajes_texto, mensajes_imagen, salas_activas = get_estadisticas() #Obtenemos la estadisticas de la sala
    sesion = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion")) #Obtenemos la cookie de sesión
    if sesion.Nombre: #Si existe sesion.Nombre
        nombre_usuario = sesion.Nombre #Nombre de usuario será igual a sesion.Nombre
    else:
        nombre_usuario = 'Anonimo' #Si no existe entonces pondremos de nombre anónimo
    home_html = render(request, "paginaPrincipal.html", {
        "lista_sala": get_sala_lista(request),
        "session": sesion,
        "text_messages": mensajes_texto,
        "image_messages": mensajes_imagen,
        "active_rooms": salas_activas,
        "user_name": nombre_usuario,
    })
    return home_html

@ensure_csrf_cookie
@csrf_exempt
def login(request): #Función para el hacer el login de nuestra página
    if request.method == "POST": #Si el metodo es POST
        password = request.POST.get("password") #Obtenemos del request el campo password y lo guardamos en la variable password
        password_exists = Password.objects.filter(Password=password).exists() #Ahora miramos si existe esta cintraseña dentro de nuestra base de datos con las contraseñas
        if password_exists: #Si existe la contraseña entonces...
            session = Sesion()
            session.Sesion = request.COOKIES.get("Sesion")
            session.save() #Nos guardamos la sesion dentro de la base de datos
            return HttpResponseRedirect("/")
    session_cookie = request.COOKIES.get("Sesion") #Obtenemos la cookie de sesión que se pasa por el request
    session_exists = Sesion.objects.filter(Sesion=session_cookie).exists() #Miramos a ver si existe esta cookie de sesion dentro de nuestra base de datos
    if session_exists: #Si la sesión existe...
        return HttpResponseRedirect("/") #Redireccion a la página principal
    login_html = render(request, "login.html") #Si la sesión no existe redireccionamos a login de nueo
    return login_html

@ensure_csrf_cookie
@csrf_exempt
def logout(request): #Función para cerrar la sesion(PARTE OPCIONAL)
    # Si el metodo es POST entonces
    if request.method == "POST":
        request.session.clear()
        response = HttpResponseRedirect("/")
        response.delete_cookie("Sesion")
        return response

#Función para obtener la página de ayuda
def help(request):
    #Checkeamos la sesion si response esta vacio es porque la sesion esta bien iniciada si no devolvemos response
    response = checkSession(request)
    if response:
        return response

    session = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion")) #Obtenemos los objetos sesion que tengan la cookie Sesion

    text_messages, image_messages, active_rooms = get_estadisticas() #
    sala_lista = get_sala_lista(request)
    help_html = render(request, "help.html", {
        "lista_salas": sala_lista,
        "session": session,
        "text_messages": text_messages,
        "image_messages": image_messages,
        "active_rooms": active_rooms
    })
    return help_html


@ensure_csrf_cookie
def config(request):
    # Verificar si la sesión del usuario está activa
    response = checkSession(request)
    if response:
        return response

    # Obtener la sesión del usuario a través de la cookie "Sesion"
    session = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion"))

    if request.method == "POST":
        # Si la solicitud es un POST, obtener los datos del formulario
        name = request.POST.get("name")
        font_size = request.POST.get("font_size")
        font_type = request.POST.get("font_type")

        # Actualizar los datos de la sesión con la información proporcionada
        session.Nombre = name
        session.font_size = font_size
        session.font_type = font_type
        session.save()

        # Renderizar la plantilla "config.html" con los datos actualizados
        response = render(request, "config.html", {
            "session": session,
            "lista_salas": get_sala_lista(request),
        })
        return response

    # Obtener algunas estadísticas
    text_messages, image_messages, active_rooms = get_estadisticas()

    # Renderizar la plantilla "config.html" con los datos de la sesión y las estadísticas
    configuration_html = render(request, "config.html", {
        "session": session,
        "lista_salas": get_sala_lista(request),
        "text_messages": text_messages,
        "image_messages": image_messages,
        "active_rooms": active_rooms,
    })

    return configuration_html



#Funcion para incluir el favicon
def favicon(request):
    with open("favicon.png", "rb") as favicon:
        ans = favicon.read()
    return HttpResponse(ans)

#Funcion que nos carga el template buscarPaginas
@ensure_csrf_cookie
def buscaPaginas(request):
    # Verificar si la sesión del usuario está activa
    response = checkSession(request)
    if response:
        return response

    # Variable para controlar si la sala no existe
    sala_no_existe = False

    # Obtener la sesión del usuario a través de la cookie "Sesion"
    session = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion"))

    if request.method == "POST":
        # Si la solicitud es un POST, obtener el nombre de la sala del formulario
        nombre_sala = request.POST.get("sala_nombre")

        if nombre_sala:
            try:
                # Intentar obtener la sala con el nombre proporcionado
                sala = Sala.objects.get(Nombre=nombre_sala)

                # Redireccionar al usuario a la página de la sala encontrada
                return HttpResponseRedirect(f'/{sala.Nombre}')

            except Sala.DoesNotExist:
                # Si la sala no existe, marcar la bandera correspondiente
                sala_no_existe = True

    # Obtener algunas estadísticas
    mensajes_texto, mensajes_imagen, salas_activas = get_estadisticas()

    # Renderizar el template "buscarPaginas.html" con los datos necesarios
    return render(request, "buscarPaginas.html", {
        'sala_no_existe': sala_no_existe,
        "session": session,
        "text_messages": mensajes_texto,
        "image_messages": mensajes_imagen,
        "active_rooms": salas_activas
    })

@csrf_exempt
def sala(request, id_sala):
    # Verificar si la sesión del usuario está activa
    response = checkSession(request)
    if response:
        return response

    # Redireccionar al usuario a la página de inicio de sesión si no hay sesión activa
    login_html_redireccion = HttpResponseRedirect("/login")

    # Obtener la sesión del usuario a través de la cookie "Sesion"
    session = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion"))

    try:
        # Obtener el objeto Sala correspondiente al ID proporcionado
        sala_objeto = Sala.objects.get(Nombre=id_sala)

        # Obtener los mensajes de la sala ordenados por fecha de manera descendente
        mensajes = Mensaje.objects.filter(Sala=sala_objeto).order_by('-Fecha')

        if request.method == "PUT":
            # Si la solicitud es un PUT, se asume que se recibirá un XML en el cuerpo de la solicitud
            xml_string = request.body.decode('utf-8')
            xml_messages = xml_parser(xml_string)
            autor, creado = Sesion.objects.get_or_create(Sesion=request.COOKIES.get("Sesion"))
            for msg in xml_messages:
                # Crear un objeto Mensaje a partir de cada mensaje en el XML y guardarlo en la base de datos
                mensaje = Mensaje(Texto=msg['text'], is_image2=msg['isimg'], Autor=autor, Sala=sala_objeto)
                mensaje.imagen2 = msg['text']
                mensaje.save()

        if request.method == "POST":
            # Si la solicitud es un POST, se espera un mensaje de texto o una imagen adjunta
            texto = request.POST.get("mensaje")
            is_image2 = bool(request.POST.get('is_image2', False))
            # image_url = request.POST.get('image_url') Esto no lo vamos a necesitar
            autor, creado = Sesion.objects.get_or_create(Sesion=request.COOKIES.get("Sesion"))
            mensaje = Mensaje(Texto=texto, is_image2=is_image2, Autor=autor, Sala=sala_objeto)
            if is_image2:
                # Si se adjunta una imagen, se guarda la URL en el campo imagen2 del objeto Mensaje
                if texto:
                    mensaje.imagen2 = texto
                    try:
                        validators.URLValidator()(mensaje.imagen2)
                    except ValidationError:
                        image_url = None
            mensaje.save()

        # Renderizar el template "sala.html" con los datos necesarios
        sala_html = render(request, "sala.html", {
            "id_sala": id_sala,
            "session": session,
            "mensajes": mensajes,
            "lista_salas": get_sala_lista(request),
        })

        return sala_html

    except Sala.DoesNotExist:
        # Si la sala no existe, redireccionar al usuario a la página de inicio de sesión
        return login_html_redireccion


#Función para la sala en formáto dinámico
@ensure_csrf_cookie
def salaDinamica(request, id_sala):
    # Verificar si la sesión del usuario está activa
    response = checkSession(request)
    if response:
        return response

    # Redireccionar al usuario a la página de inicio de sesión si no hay sesión activa
    login_html_redireccion = HttpResponseRedirect("/login")

    # Obtener la sesión del usuario a través de la cookie "Sesion"
    session = Sesion.objects.get(Sesion=request.COOKIES.get("Sesion"))

    try:
        # Obtener el objeto Sala correspondiente al ID proporcionado
        sala_objeto = Sala.objects.get(Nombre=id_sala)

        # Obtener los mensajes de la sala ordenados por fecha de manera descendente
        mensajes = Mensaje.objects.filter(Sala=sala_objeto).order_by('-Fecha')

        if request.method == "PUT":
            # Si la solicitud es un PUT, se asume que se recibirá un XML en el cuerpo de la solicitud
            xml_string = request.body.decode('utf-8')
            xml_messages = xml_parser(xml_string)
            autor, creado = Sesion.objects.get_or_create(Sesion=request.COOKIES.get("Sesion"))
            for msg in xml_messages:
                # Crear un objeto Mensaje a partir de cada mensaje en el XML y guardarlo en la base de datos
                mensaje = Mensaje(Texto=msg['text'], is_image2=msg['isimg'], Autor=autor, Sala=sala_objeto)
                mensaje.imagen2 = msg['text']
                mensaje.save()

        if request.method == "POST":
            # Si la solicitud es un POST, se espera un mensaje de texto o una imagen adjunta
            texto = request.POST.get("mensaje")
            is_image2 = bool(request.POST.get('is_image2', False))
            # image_url = request.POST.get('image_url') Esto no lo vamos a necesitar
            autor, creado = Sesion.objects.get_or_create(Sesion=request.COOKIES.get("Sesion"))
            mensaje = Mensaje(Texto=texto, is_image2=is_image2, Autor=autor, Sala=sala_objeto)
            if is_image2:
                # Si se adjunta una imagen, se guarda la URL en el campo imagen2 del objeto Mensaje
                if texto:
                    mensaje.imagen2 = texto
                    try:
                        validators.URLValidator()(mensaje.imagen2)
                    except ValidationError:
                        image_url = None
            mensaje.save()

        # Renderizar el template "sala.html" con los datos necesarios
        sala_html = render(request, "salaDinamica.html", {
            "id_sala": id_sala,
            "session": session,
            "mensajes": mensajes,
            "lista_salas": get_sala_lista(request),
        })

        return sala_html

    except Sala.DoesNotExist:
        # Si la sala no existe, redireccionar al usuario a la página de inicio de sesión
        return login_html_redireccion

#Función para obtener una sala en formato json
def salaJson(request, id_sala):
    # Verificar si la sesión del usuario está activa
    response = checkSession(request)
    if response:
        return response

    try:
        # Obtener el objeto Sala correspondiente al ID proporcionado
        sala = Sala.objects.get(Nombre=id_sala)

        # Obtener los mensajes de la sala ordenados por fecha de manera descendente
        mensajes = Mensaje.objects.filter(Sala=sala).order_by('-Fecha')

        messages_json = []
        for mensaje in mensajes:
            # Crear un diccionario con los datos del mensaje para generar la estructura JSON
            message_data = {
                "author": mensaje.Autor.Nombre,
                "text": mensaje.Texto,
                "isimg": mensaje.is_image2,
                "date": mensaje.Fecha.strftime("%Y-%m-%d %H:%M:%S")
            }
            messages_json.append(message_data)

        # Convertir la lista de mensajes en formato JSON y retornarlo como respuesta
        return HttpResponse(json.dumps(messages_json), content_type="application/json")

    except Sala.DoesNotExist:
        # Si la sala no existe, retornar un código de estado HTTP 404 (Recurso no encontrado)
        return HttpResponse(status=404)

#Función para borrar una sala
@ensure_csrf_cookie
def deleteSala(request, id_sala):
    # Verificar si la sesión del usuario está activa
    response = checkSession(request)
    if response:
        return response

    try:
        # Obtener el objeto Sala correspondiente al ID proporcionado
        sala = Sala.objects.get(id=id_sala)

        # Eliminar la sala
        sala.delete()

    except Sala.DoesNotExist:
        # Si la sala no existe, no se realiza ninguna acción

        pass

    # Redirigir al usuario a la página principal
    return HttpResponseRedirect("/")

#Función para votar las salas
def voto(request, id_sala):
    # Verificar si la sesión del usuario está activa
    response = checkSession(request)
    if response:
        return response

    try:
        # Obtener la sala correspondiente al ID proporcionado
        salaVoto = Sala.objects.get(id=id_sala)

        # Incrementar el contador de votos en 1
        salaVoto.Votos += 1

        # Guardar los cambios en la sala
        salaVoto.save()

    except Sala.DoesNotExist:
        # Si la sala no existe, redirigir al usuario a la página principal
        return HttpResponseRedirect("/")

    # Redirigir al usuario a la página principal
    return HttpResponseRedirect("/")
